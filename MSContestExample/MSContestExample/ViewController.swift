//
//  ViewController.swift
//  MSContestExample
//
//  Created by Morgan Le Gal on 10/05/2021.
//

import MSContests
import UIKit

class ViewController: UIViewController {

    private lazy var openContestButton: UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.setTitle("Open Contests", for: .normal)
        b.setTitleColor(.systemBlue, for: .normal)
        b.setTitleColor(UIColor.systemBlue.withAlphaComponent(0.7), for: .normal)
        b.addTarget(self, action: #selector(didTapOpenContests), for: .touchUpInside)
        return b
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        
        view.addSubview(openContestButton)
        
        NSLayoutConstraint.activate([
            openContestButton.widthAnchor.constraint(equalToConstant: 150),
            openContestButton.heightAnchor.constraint(equalToConstant: 50),
            openContestButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            openContestButton.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ])
    }

    @objc
    private func didTapOpenContests() {
        let center = MSContestsCenter()
        if let controller = center.currentViewController {
            present(controller, animated: true)
        }
    }

}

