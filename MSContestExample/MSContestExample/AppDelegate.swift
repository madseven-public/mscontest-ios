//
//  AppDelegate.swift
//  MSContestExample
//
//  Created by Morgan Le Gal on 10/05/2021.
//
import MSContests
import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        // Initialize the SDK with your API Key and Secret
        MSContest.shared.initialize(
            clientId: "your-client-id",
            clientSecret: "your-client-secret",
            linkAppUser: true
        )
        MSContest.shared.delegate = self
        
        let theme = MSTheme()
        theme.backgroundColor = .white
        theme.shouldDisplaySponsorInList = false
        theme.italicFontName = "AvenirNext-Italic"
        theme.regularFontName = "AvenirNext-Regular"
        theme.mediumFontName = "AvenirNext-Medium"
        theme.boldFontName = "AvenirNext-Bold"
        theme.presentOverFullScreen = false
        theme.displayTimerFromDay = 3
        MSContest.shared.configureTheme(theme)
                
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}


// MARK: - MSContestDelegate

extension AppDelegate: MSContestDelegate {
    
    func userInfoRequired(presentingController: UIViewController) {
        doShowConnectError(
            presentingController: presentingController,
            title: "Login",
            message: "Your user needs to be linked to participate to a contest.",
            actionButton: "Link User",
            actionHandler: {
            MSContest.shared.linkUser(
                id: 1,
                email: "john@hodw.co",
                username: "johnbo"
            )
        })
    }
    
    private func doShowConnectError(
        presentingController: UIViewController,
        title: String,
        message: String,
        actionButton: String,
        actionHandler: @escaping (() -> Void)
    ) {
        
        let alert = UIAlertController(
            title: title,
            message: message,
            preferredStyle: .alert
        )
        
        alert.addAction(
            UIAlertAction(
                title: "Cancel",
                style: .default,
                handler: nil
            )
        )
        
        alert.addAction(
            UIAlertAction(
                title: actionButton,
                style: .default,
                handler: { _ in
                    actionHandler()
                }
            )
        )
        
        presentingController.present(alert, animated: true)
    }
}

