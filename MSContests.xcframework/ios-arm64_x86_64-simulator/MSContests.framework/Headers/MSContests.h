//
//  MSContests.h
//  MSContests
//
//  Created by Morgan Le Gal on 19/04/2021.
//

#import <Foundation/Foundation.h>

//! Project version number for MSContests.
FOUNDATION_EXPORT double MSContestsVersionNumber;

//! Project version string for MSContests.
FOUNDATION_EXPORT const unsigned char MSContestsVersionString[];

#import <CommonCrypto/CommonCrypto.h>
