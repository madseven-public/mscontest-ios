# MSContest-iOS

![iOS 11.0+](https://img.shields.io/badge/iOS-11.0%2B-blue.svg)

### Presentation

**MSContest** is a contests library. It displays a list of contests and allow the user to participate to it.

## Updating the Framework

Once you've updated the framework and tested that everything works as expected, you'll have to run the script `generate-xcframework.sh`:

From the terminal, go in the repo directory:
```swift
./generate-xcframework.sh
```

After few seconds, you should see that the XCFramework have successfully been created in folder that you're in.

Take this newly created framework and drag it to the [public repository](https://gitlab.com/madseven-public/mscontest-ios), push the new framework to the remote branch. 

⚠️ Make sure to create a new tag to keep track of the framework version.

## Usage & Installation

See public repository [here](https://gitlab.com/madseven-public/mscontest-ios).
