**MSContests** is a library for displaying and participating to contests.

![Xcode 12.0+](https://img.shields.io/badge/Xcode-12.0%2B-blue.svg)
![iOS 11.0+](https://img.shields.io/badge/iOS-11.0%2B-blue.svg)
![Swift 5.3+](https://img.shields.io/badge/Swift-5.3%2B-orange.svg)


# Quick Start Guide


## Installation

### CocoaPods

Add the following entry to your Podfile:

```rb
pod 'MSContests'
```

Then run `pod install`.

Don't forget to `import MSContests` in every file you'd like to use MSContests.

### Swift Package Manager

To integrate using Apple's Swift package manager, add the following as a dependency to your `Package.swift`:

```swift
.package(url: "https://gitlab.com/madseven-public/mscontests-ios.git", .upToNextMajor(from: "1.0.0"))
```

and then specify `"MSContests"` as a dependency of the Target in which you wish to use MSContests.
Here's an example `PackageDescription`:

```swift
// swift-tools-version:4.0
import PackageDescription

let package = Package(
    name: "MyPackage",
    products: [
        .library(
            name: "MyPackage",
            targets: ["MyPackage"]),
    ],
    dependencies: [
        .package(url: "https://gitlab.com/madseven-public/mscontests-ios.git", .upToNextMajor(from: "1.0.0"))
    ],
    targets: [
        .target(
            name: "MyPackage",1.0.0
            dependencies: ["MSContests"])
    ]
)
```

### Manually

1. Drag and drop MSContests.xcframework to your Xcode project. When prompted “Choose options for adding these files:” make sure **Copy items if needed** and your project target is selected. 

2. We need now to set new paths in the **Import Paths** of the target's build settings. Providing the path to the directory where each and every `.swiftmodule` directories are will allow your application to interact with the static library.
Add the following paths:
* `$(PROJECT_DIR)/path-to-framework/MSContests.xcframework/ios-arm64`
* `$(PROJECT_DIR)/path-to-framework/MSContests.xcframework/ios-x86_64-simulator`


## Integration

### Initialization

There is two way to initialize the SDK.

#### 1. Linking your User to our SDK 

In your `AppDelegate`, in the method `didFinishLaunchingWithOptions`, add the following :

```swift
MSContest.shared.initialize(
    clientId: "your-client-id",
    clientSecret: "your-client-secret",
    linkAppUser: true
)
```

To listen to our SDK events, sets the delegate, where you find it appropriate, in our example it will be right after the `initialize` method in the `AppDelegate`:

```swift
MSContest.shared.delegate = self 
```

And

```swift
extension AppDelegate: MSContestDelegate {
    func userInfoRequired(presentingController: UIViewController) { }
}
```

To link your User to our SDK, you will need to call the method `linkUser(id: Int?, mail: String?, username: String?)` once your user is connected to your app.

```swift
MSContest.shared.linkUser(
    id: 12,
    mail: "user@mail.co",
    username: "username"
)
```

A User can still navigate through the contests list even if he's not connected. 

When a User not logged in clic on the `participate` button, the method `userInfoRequired(presentingController: UIViewController)` from the delegate, will be called.
From this method you can present your login flow and link the user, once connected, to our SDK.

```swift 
func userInfoRequired(presentingController: UIViewController) {
    /* 
    Present your login flow from the presentingController...
    
    Once the flow is completed and your user is logged in, you can link the User to our SDK.
    */
    MSContest.shared.linkUser(
        id: 12,
        mail: "user@mail.co",
        username: "username"
    )
}
```

If your login process does not include `mail` or `username`, that's not a problem, we will present to the User a login form pre-filled with the information that we have and asking her/him to fill the missing ones when he or she will try to participate to a contest.
All the parameters from the method `linkUser` are optionals.

If the **user logs out** from your main application you can unlink the user from the SDK as well with:

```swift
MSContest.shared.unlinkUser()
```

#### 2. Using our login solution

In your `AppDelegate`, in the method `didFinishLaunchingWithOptions`, add the following :

```swift
MSContest.shared.initialize(
    clientId: "your-client-id",
    clientSecret: "your-client-secret"
)
```
That's it. We will take care of the rest when needed.


### Displaying the contest

To present the contest list, simply add:

```swift
let center = MSContestsCenter()
if let controller = center.currentViewController {
   present(controller, animated: true)
}
```

You can present it modally, push it in a navigation controller or set it as a tab item of your tabBarController.

### Registering push token to receive notifications

In order to be able to receive push notifications you need to register your push token to the SDK.

```swift
MSContest.shared.registerPushToken("your-push-token")
```

### Debugging 

You can display the SDK logs with: 

```swift
MSContest.shared.isDebugEnabled = true 
```


### Customize the interface

After initializing the SDK you can customize the look by creating a new instance of `MSTheme` and pass that same object to the SDK using the `MSContest.shared.configureTheme(_ theme: MSTheme)` method.

```swift
let theme = MSTheme()

theme.primaryColor = .red
theme.backgroundColor = .white
theme.textColor = .black
theme.italicFontName = "AvenirNext-Italic"
theme.regularFontName = "AvenirNext-Regular"
theme.mediumFontName = "AvenirNext-Medium"
theme.boldFontName = "AvenirNext-Bold"
theme.appLogo = UIImage(named: "appLogo")

theme.canParticipateInList = true 
theme.shouldDisplaySponsorInList = false
theme.presentOverFullScreen = true
theme.isCardMode = false 
theme.displayTimerFromDay = 3

MSContest.shared.configureTheme(theme)
```

#### Available parameters 

| Params      | Description | Default value |
| ----------- | ----------- | ----------- |
| appLogo | Your application logo. Will be displayed in login and user info screens | nil |
| primaryColor | The main color of your App, will mainly appear on buttons and emphasized texts | #027AFF |
| backgroundColor | Changes the background color of the different screens | #FFFFFF |
| textColor | Changes the color of texts | #3C3C3C |
| italicFontName | The name of the font for italic texts | Italic system font |
| regularFontName | The name of the font for regular texts | System font |
| mediumFontName | The name of the font for medium weight texts | System font medium weight |
| boldFontName | The name of the font for bold texts | System Font bold weight |
| canParticipateFromList | Display the participate button in the contests list | true |
| displayTimerFromDay | Remaining days before contest end to display a countdown timer. | 2 |
| shouldDisplaySponsorInList | Display the sponsor icon in the contests list | true |
| isCardMode | Display cards in contests list with cornerRadius | true |
| presentOverFullScreen | Present any modal screen over full screen | true |
