Pod::Spec.new do |s|
  s.name        = "MSContests"
  s.version     = "1.0.1"
  s.summary     = "MadSeven's Contest framework"
  s.description = "This framework provides contests. You can easily display the contests and participate to it."
  s.homepage    = "https://gitlab.com/madseven-public/mscontest-ios"
  s.license     = { :type => 'Apache-2.0', :file => 'LICENSE' }
  s.authors     = "MadSeven"

  s.platform = :ios, "11.0"
  s.swift_version = "5.0"
  
  s.source   = { :git => "https://gitlab.com/madseven-public/mscontest-ios.git", :tag => s.version }
  s.vendored_frameworks   = 'MSContests.xcframework'


end
